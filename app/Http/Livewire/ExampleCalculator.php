<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ExampleCalculator extends Component
{
    public float $result = 0.00;

    public $loanAmount;

    public $interestRate;

    public int $termDuration = 1;

    public string $frequency = 'Month';

    protected array $frequencies = ['Month', 'Fortnight', 'Week'];

    protected array $rules = [
        'result' => '',
        'loanAmount' => 'required|numeric|min:1000',
        'interestRate' => 'required|numeric|min:0|max:100',
        'termDuration' => 'required|integer|min:1|max:5',
        'frequency' => ''
    ];

    public function setFrequency($freq)
    {
        //Check to ensure the frequency is valid from the buttons
        //if it is not default it back to Month
        if (!in_array($freq, $this->frequencies))
            $this->frequency = 'Month';

        //Set the frequency to the chosen selection
        $this->frequency = $freq;

        //Run the calculation
        $this->calculateRate();
    }

    public function calculateRate()
    {
        //Validate the inputs
        $this->validate();

        // Calculate the estimated repayment for a loan, based on the loanAmount, interestRate, termDuration,
        // and display the amount in the selected frequency.

        //Get the number of months
        $months = (int) $this->termDuration * 12;

        //Loan Amount
        $pV = (float) $this->loanAmount;

        //It is not interest free (the more likely scenario)
        if ((float) $this->interestRate != 0) {
            $calcInterestRate = $this->interestRate / 100;
            $monthlyRate = $calcInterestRate / 12;

            //Monthly Interest Rate
            $i = $monthlyRate;

            //Power of
            $powerOf = pow(1 + $i, $months);

            $top = $pV * $i * $powerOf;
            $bottom = $powerOf - 1;

            $monthlyPay = $top / $bottom;
        } else {
            //It is interest free, so just divide up the payment by the months
            $monthlyPay = $pV / $months;
        }

        switch ($this->frequency) {
            case 'Week':
                $this->result = $monthlyPay / 4;
                break;
            case 'Fortnight':
                $this->result = $monthlyPay / 2;
                break;
            default:
                $this->result = $monthlyPay;
                break;
        }
    }

    public function resetForm()
    {
        $this->reset('result', 'loanAmount', 'interestRate', 'termDuration', 'frequency');
    }

    public function render()
    {
        return view('livewire.example-calculator');
    }
}
